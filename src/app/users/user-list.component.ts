// TODO SOMEDAY: Feature Componetized like CrisisCenter
import 'rxjs/add/operator/switchMap';
import {Observable} from 'rxjs/Observable';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';

import {User, UserService} from './user.service';

@Component({
    templateUrl: './user-list.component.html'
})
export class UserListComponent implements OnInit {
    users: User[];

    constructor(private service: UserService) {
    }

    ngOnInit() {
        this.service.getUsers()
            .subscribe(
                users => this.users = users,
                err => console.log(err)
            );
    }
}
